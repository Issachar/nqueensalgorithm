package work;

class OcticState
{
	//Initialize Bishop set
	private int[] alpha;
	private int[] beta;
	private int[] omega;
	private int[] pi;
	
	//Initialize Rook set
	private int[] alphaPrime;
	private int[] betaPrime;
	private int[] omegaPrime;
	private int[] piPrime;
	
	//Constructor
	public OcticState()
	{
		this.alpha     = new int[7];
		this.beta      = new int[7];
		this.omega     = new int[7];
		this.pi        = new int[7];
		this.alphaPrime= new int[7];
		this.betaPrime = new int[7];
		this.omegaPrime= new int[7];
		this.piPrime   = new int[7];
	}

	/*** Use formulae from set theory****/
	public int getAlpha(int index)
	{
		return alpha[index];
	}
	public int[] getAlpha(int ga[])
	{
		//int k
		
		return null;
	} 
	
	public void setAlpha(int index, int value)
	{
		alpha[index] = value;
	}

	public int getBeta(int index)
	{
		return beta[index];
	}
	
	public void setBeta(int index, int value)
	{
		beta[index] = value;
	}

	public int getOmega(int index)
	{
		return omega[index];
	}
	
	public int[] getOmega()
	{
		return omega;
	}
	
	public void setOmega(int index, int value)
	{
		omega[index] = value;
	}

	public int getPi(int index)
	{
		return pi[index];
	}

	public int[] getPi()
	{
		return pi;
	}
	
	public void setPi(int index, int value)
	{
		pi[index] = value;
	}

	public int getAlphaPrime(int index)
	{
		return alphaPrime[index];
	}
	
	public void setAlphaPrime(int index, int value)
	{
		alphaPrime[index] = value;
	}

	public int getBetaPrime(int index)
	{
		return betaPrime[index];
	}
	
	public void setBetaPrime(int index, int value)
	{
		betaPrime[index] = value;
	}

	public int getOmegaPrime(int index)
	{
		return omegaPrime[index];
	}
	
	public int[] getOmegaPrime()
	{
		return omegaPrime;
	}
	
	public void setOmegaPrime(int index, int value)
	{
		omegaPrime[index] = value;
	}

	public int getPiPrime(int index)
	{
		return piPrime[index];
	}
	
	public void setPiPrime(int index, int value)
	{
		piPrime[index] = value;
	}

}
