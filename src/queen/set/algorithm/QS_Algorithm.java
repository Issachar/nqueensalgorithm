package queen.set.algorithm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Stack;



public class QS_Algorithm
{
    
    //Class fields
    public  static  Stack<Integer> stack=new Stack<>();
    public  static  ModChessBoard modBoard;
    public  static  ModChessBoard modBoard2;
    public  static  ModChessBoard modBoard3;
    private static int[][] plcBoard;
    private static int boardDimension;
    static  ArrayList<Integer> search1= new ArrayList<>();
    static  ArrayList<Integer> search2 = new ArrayList<>();
    static  ArrayList<Integer> search3 = new ArrayList<>();
    static  ArrayList<Integer> array = new ArrayList<>();
    //static  ArrayList<Integer> stc = new ArrayList<>();
    
    private static int[] setLHolder =null; 
 
    private static int Bi=0;
    private static int Bj=0;
    private static int Pr=0;
    private static int Pc=0;
    private static int count=0;
    
  //  ArrayList<QS_Algorithm> array=new ArrayList();                    
    
    Scanner sc=new Scanner(System.in);
    
   //Class constructor
   public QS_Algorithm()
   {
   
	  System.out.print("Enter board Dimension 00-14:");
	  int bDim=sc.nextInt();
	
	  boardDimension=bDim;
	  modBoard=new ModChessBoard(bDim);
          modBoard2=new ModChessBoard(bDim);
          modBoard3=new ModChessBoard(bDim);
	  plcBoard=new int[bDim][bDim];  
	  
	     for(Bi=0; Bi<bDim;Bi++)
	    	for(Bj=0; Bj<bDim;Bj++)
	    		plcBoard[Bi][Bj]=0;
	    
	   placeBoard(0,0);
           placeBoard(2,1);
           start(); 
   }
   
  
   public void getBoard()
   {
	 modBoard.printBoard();
   }
 
   public static void start()
   {
       solve(count,Pr+2,Pc+1);
   }
   public static boolean solve(int numQueens,int pr,int pc)
    {
      int pos1;
      int pos2;
        
        if(numQueens==6)
       {
            System.out.println("Done");
           //printBoard();
            return true;    
       }
       else
       {
           ArrayList<Integer> stc=constrainSetG(pr,pc);
         if(stc.isEmpty())
             return true;
           //for(int i=0;i<arrayQ.size();i++)
            //  stc.add(arrayQ.get(i));
            
           pos1=(int) modBoard2.getBoardElmsIndex(stc.get(0)).get(0);
           pos2=(int) modBoard2.getBoardElmsIndex(stc.get(0)).get(1);
            
           printBoard();
           System.out.println(Arrays.toString(stc.toArray()));
           numQueens++;
        placeBoard(pos1,pos2);
        solve(numQueens,pos1,pos2);
       } 
       return false;
    }   
  
  public ArrayList<Integer> placeQueen(int pr,int pc)
   {
     return constrainSetG(pr,pc);
   } 

  public static void placeBoard(int Pr,int Pc)
  {
       plcBoard[Pr][Pc]=1;
  }
   
   public static ArrayList<Integer> constrainSetG(int i,int j) 
   {
      array=modBoard.generateSetG(i,j);
         for(int x=0;x<boardDimension;x++)
             for(int y=0;y<boardDimension;y++)
               if(plcBoard[x][y]==1)
               {
               search1=modBoard.getPi(x , y);
               search2=modBoard2.getOmega(x , y);
               search3=modBoard3.getOmegaPrime(x , y);
               
               Iterator it1=array.iterator();
               Iterator it2=search2.iterator();
               Iterator it3=search3.iterator();
               
               while(it2.hasNext())
                   search1.add((Integer)it2.next());
               
               while(it3.hasNext())
                   search1.add((Integer)it3.next());
               
               while(it1.hasNext())
                  if(search1.contains((Integer)it1.next()))
                    it1.remove();
            
               }
            
     return array;
    }
   
   public static int get(int x, int y)
   {
       if( x < 0 || y < 0 || x > boardDimension-1 || y > boardDimension-1)
       {
       return -1;
       }
     return plcBoard[x][y];
   }


   public static void printBoard()
   {
     int[][] a=new int[boardDimension][boardDimension];
     
     for(int i=0; i<boardDimension; i++)
     {
        for(int j=0; j<boardDimension; j++)
        {
          a[i][j]=get(i,j);      
        System.out.printf("%3d",a[i][j]);
        }
      System.out.println();
     }
   }
  
  
}


