package work;

class Board<E>
{
	private E[] grid;
	private int numRows;
	private int numCols;
	
	@SuppressWarnings("unchecked")
	public Board(int m, int n)
	{
		this.numRows = m;
		this.numCols = n;
		this.grid = (E[]) new Object[m * n];
	}
	
	public int getNumRows()
	{
		return this.numRows;
	}
	
	public int getNumCols()
	{
		return this.numCols;
	}
	
	public int getSize()
	{
		return grid.length;
	}
	
	public void set(int i, int j, E value)
	{
		grid[i * numCols + j] = value;
	}
	
	public void set(int i, E value)
	{
		grid[i] = value;
	}
	
	public E get(int i, int j)
	{
        return grid[i * numCols + j];
	}
	
	public E get(int i)
	{
          return grid[i];
	}
    
	public E[] string()
	{
       return this.grid;		
	}
}


