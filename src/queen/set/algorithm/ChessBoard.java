package queen.set.algorithm;
	
class ChessBoard{
		private Board<OcticState> board8x8;
		
		public ChessBoard()
		{
			board8x8 = new Board<OcticState>(8,8);
			initOcticState();
		}
		
		protected void initOcticState()
		{
			for (int i = 0; i < board8x8.getSize(); i++)
				board8x8.set(i, new OcticState());
		}
		
		public OcticState getOcticState(int i, int j)
		{
			return board8x8.get(i, j);
		}

		public OcticState getOcticState(int i)
		{
			return board8x8.get(i);
		}
		
		public void setOcticState(int i, int j, OcticState chessSet)
		{
			board8x8.set(i, j, chessSet);
		}
		
		public void setOcticState(int i, OcticState chessSet)
		{
			board8x8.set(i, chessSet);
		}
		
		public OcticState getOcticState()
		{
			
		}
	}


