package queen.set.algorithm;
import java.util.ArrayList;

public class ModChessBoard 
{   //Initialize Bishop set:one-dimensional array
	private static ArrayList alpha=new ArrayList();
	private static ArrayList beta=new ArrayList();
	private ArrayList<Integer> omega=new ArrayList<>();
	private ArrayList<Integer> pi=new ArrayList<>();
	
   //Initialize Rook set:one-dimensional array
	private static ArrayList alphaPrime=new ArrayList();
	private static ArrayList betaPrime=new ArrayList();
	private ArrayList<Integer> omegaPrime=new ArrayList<>();
	private static ArrayList piPrime=new ArrayList();
   
     //Method local variable declaration 
        //int[] posHolder=new int[2];
        ArrayList<Integer> posHolder=new ArrayList<>(); 
       // ArrayList<Integer> setLHolder=new ArrayList<>();
        
     //Class fields	 
	private static int[][] board=null;
        private static int boardDim;
        private static int bl=0;
        public static int numPlacedQueens=0;
        int k=0;
        int r=0;
        int c=0; 
        
        int t=0;
        int v=0;
   public ModChessBoard(int bDim)
   {
	int elmsPos=0;
        boardDim=bDim;
    
        board=new int[bDim][bDim]; 
	bl=board.length;     
    
	     int[] uniqueElms=new int[bDim*bDim];
	     
	     for(int i=0;i<bDim*bDim;i++)
	     {
	      uniqueElms[i]=i+1;
	     }
	     
	     for(int j=0;j<bDim;j++)
	       for(int k=0;k<bDim;k++)
	       {
	         board[j][k]=uniqueElms[elmsPos];
	         elmsPos++;
	       }
    }

   /**Octic states from Queen-Set system used for the eight getters below **/
   
    protected boolean inBounds(int i, int j)
    {
       return 	i >= 0 && i < this.bl  &&  j >= 0 && j < this.bl;
    } 
	
  
    protected ArrayList getAlpha(int i,int j)
    {
       alpha.add(boardPosIterator(i-r,j-c));
       r++;
       c++;
      return(inBounds(i-r,j-c))?getAlpha(i--,j--):alpha;
    } 

   protected ArrayList getBeta(int i,int j)
   {
       beta.add(boardPosIterator(i+r,j-c));
       r++;
       c++;
      return(inBounds(i+r,j-c))?getBeta(i++,j--):beta;
   }

   protected ArrayList<Integer> getPi(int i,int j)
   {
       pi.add(boardPosIterator(i-r,j+c));
       r++; 
       c++;
      return(inBounds(i-r,j+c))?getPi(i--,j++):pi;
   }
	 
   protected ArrayList getOmega(int i,int j)
   {
	omega.add(boardPosIterator(i+r,j+c));
        r++;
        c++;
       return (inBounds(i+r,j+c))? getOmega(i++,j++):omega;
   }
		
   protected ArrayList getAlphaPrime(int i,int j)
   {  
        alphaPrime.add(boardPosIterator(i,j-c));
        c++;
       return(inBounds(i,j-c))?getAlphaPrime(i,j--):alphaPrime;
   }

   protected ArrayList getBetaPrime(int i,int j)
   {
        betaPrime.add(boardPosIterator(i+r,j));
        r++;
       return(inBounds(i+r,j))?getBetaPrime(i++,j):betaPrime;
   }
	
   protected ArrayList getOmegaPrime(int i,int j)
   {
	omegaPrime.add(boardPosIterator(i,j+c));
        c++;
       return(inBounds(i,j+c))?getOmegaPrime(i,j++):omegaPrime;
   }

   protected ArrayList getPiPrime(int i,int j)
   {
	try{
        piPrime.add(boardPosIterator(i-r,j));
        k++;
        r++;}
        catch(ArrayIndexOutOfBoundsException e)
        {
         e.printStackTrace();
        }
       return(inBounds(i-r,j))?getPiPrime(i--,j):piPrime;
   }
	
   protected ArrayList computeSetL(int i,int j)
   {
      ArrayList<Integer> setLHolder=new ArrayList<>();
     if(inBounds(i-1,j-1)) 
      setLHolder.add(board[i-1][j-1]);
    
     if(inBounds(i,j-1)) 
      setLHolder.add(board[i][j-1]);
     
     if(inBounds(i+1,j-1)) 
      setLHolder.add(board[i+1][j-1]);
          
     return setLHolder;
   }
   
   protected ArrayList generateSetG(int i,int j) 
   {
      ArrayList<Integer> setHolder=computeSetL(i,j);
      ArrayList<Integer> setGHolder=new ArrayList<>();
       
      for(int u=0; u<setHolder.size();)
       { 
           posHolder=getBoardElmsIndex(setHolder.get(u));
            
           if(inBounds(posHolder.get(0),posHolder.get(1)) && (i-1==posHolder.get(0)))
           {
              if(inBounds(posHolder.get(0)-2,posHolder.get(1)+2))  
                   setGHolder.add(board[posHolder.get(0)-2][posHolder.get(1)+2]);
           }
           
           if(inBounds(posHolder.get(0),posHolder.get(1)) && (i-posHolder.get(0)==0))
           {
              if(inBounds(posHolder.get(0)-2,posHolder.get(1)+2)) 
                  setGHolder.add(board[posHolder.get(0)-2][posHolder.get(1)+2]);
              
              if(inBounds(posHolder.get(0)+2,posHolder.get(1)+2))
                  setGHolder.add(board[posHolder.get(0)+2][posHolder.get(1)+2]);
           }
           
           if(inBounds(posHolder.get(0),posHolder.get(1)) && (i+1==posHolder.get(0)))
           {
              if(inBounds(posHolder.get(0)+2,posHolder.get(1)+2)) 
                 setGHolder.add(board[posHolder.get(0)+2][posHolder.get(1)+2]);
           }    
         u++;
       }    
       return setGHolder;
   }
   

   /**
    * This method returns the index [][] of an element
    * @param int elms
    * @return ArrayList
    */
   public ArrayList getBoardElmsIndex(int elms)
   {
      ArrayList pos=new ArrayList(); 
       for(int u=0; u<bl ;u++)
           for(int v=0; v<bl ;v++)
           {
             if(board[u][v]==elms)
             {
                pos.add(u);
                pos.add(v);
             }
           }
       return pos;
   }
  
    public int[] getBoardElmsIndex2(int elms)
   {
      int[] pos=new int[2]; 
       for(int u=0; u<bl ;u++)
           for(int v=0; v<bl ;v++)
           {
             if(board[u][v]==elms)
             {
                pos[0]=u;
                pos[1]=v;
             }
           }
       return pos;
   }
    
   public int boardPosIterator(int i,int j)
   {
      //return board[i][j];
       return(inBounds(i,j))?board[i][j]:-1;
   }
	
   public static int get(int x, int y)
   {
       if( x < 0 || y < 0 || x > boardDim-1 || y > boardDim-1 )
	 {
	  return -1;
	 }
       return board[x][y];
   }
	    
   public void printBoard()
   {
        int[][] a=new int[boardDim][boardDim];

           for(int i=0; i<boardDim; i++){
               for(int j=0; j<boardDim; j++){
                   a[i][j]=get(i,j);
                System.out.printf("%3d",a[i][j]);
                }
            System.out.println();
           }
	       
   }
	    
}
